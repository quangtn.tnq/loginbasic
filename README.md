# Introduction
UserName: `userName1`
Password: `123456`

Data Mẫu: `NetCore/NetCore/Database/Profil.json`

API URL: `https://localhost:5200`

SignIn URL: `https://localhost:5200/Api/Profil/SignIn`
```typescript
    curl --location --request POST 'https://localhost:5165/Api/Profil/SignIn' \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "userName": "userName1",
        "password": "e10adc3949ba59abbe56e057f20f883e"
    }'
```

Profil URL: `https://localhost:5200/Api/Profil/1`
```typescript
    curl --location --request GET 'https://localhost:5200/Api/Profil/1' \
    --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiMSIsIm5iZiI6MTY2MTMzODkzOCwiZXhwIjoxNjYzOTMwOTM4LCJpYXQiOjE2NjEzMzg5Mzh9.X-2cdpKdWwW00J3NkopSgOpdanYJmww1pLS6fsNOyjo'
```
