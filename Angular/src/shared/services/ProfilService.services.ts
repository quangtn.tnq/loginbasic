import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
// import * as jwt_decode from 'jwt_decode';
import { ResponseModel } from '../models/Response.model';
import { LoginModel } from '../models/Login.model';
import { BaseService } from './Base.service';
import { VerifyOAuthModel } from '../models/VerifyOAuth.model';

@Injectable({
  providedIn: 'root'
})
export class ProfilService extends BaseService {

  constructor(
    protected override httpClient: HttpClient,
  ) {
    super(httpClient, `https://localhost:5200/Api`, 'Profil');
  }

  async login(model: LoginModel): Promise<ResponseModel> {
    const res = await this.postCustomApi('SignIn', model);

    if (!res.e) {
      localStorage.setItem(`expires`, res.d.Expires);
      localStorage.setItem(`isLoginIn`, 'true');
      localStorage.setItem(`token`, res.d.Token);
      localStorage.setItem(`userId`, res.d.UserId);
      localStorage.setItem(`fullName`, res.d.FullName);
    }

    return res;
  }
  async getDataById(): Promise<ResponseModel> {
    const userId = localStorage.getItem(`userId`);

    const res = await this.getCustomApi(`${userId}`, new HttpParams());
    return res;
  }
  async createOAuth(): Promise<ResponseModel> {
    const userId = localStorage.getItem(`userId`);

    let params = new HttpParams();
    params = params.append(`id`, `${userId}`);

    const res = await this.getCustomApi('CreateOAuth', params);
    return res;
  }
  async verifyOAuth(model: VerifyOAuthModel): Promise<ResponseModel> {
    const userId = localStorage.getItem(`userId`);
    model.userId = userId;
    
    const res = await this.postCustomApi('VerifyOAuth', model);
    return res;
  }

  isLogged() {
    const token = localStorage.getItem(`token`);
    const isVerifyOAuth = localStorage.getItem(`isVerifyOAuth`);
    if (token && isVerifyOAuth) {
    //   const jwt = jwt_decode(token);
    //   if (jwt.exp * 1000 > new Date().valueOf()) {
    //   }
      return true;
    }
    return false;
  }

  getUserId(): string {
    return localStorage.getItem(`userId`) || ``;
  }

  getFullName(): string {
    return localStorage.getItem(`fullName`) || ``;
  }

  async logout() {
    localStorage.clear();
  }
}
