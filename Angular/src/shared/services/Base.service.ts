import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject } from '@angular/core';
import { ResponseModel } from '../models/Response.model';

// @Injectable()
export class BaseService {
    constructor(
        protected httpClient: HttpClient,
        @Inject(String) protected urlName: string,
        @Inject(String) protected apiName: string
    ) {
    }

    async getCustomApi(apiMethod: string, params: HttpParams): Promise<ResponseModel> {
        return await this.httpClient.get<ResponseModel>(`${this.urlName}/${this.apiName}/${apiMethod}`, { params }).toPromise();
    }

    async postCustomApi(apiMethod: string, model: object): Promise<ResponseModel> {
        return await this.httpClient.post<ResponseModel>(`${this.urlName}/${this.apiName}/${apiMethod}`, model).toPromise();
    }
}
