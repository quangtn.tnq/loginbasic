import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/do';

@Injectable()
export class ApiEndpointInterceptor implements HttpInterceptor {
    constructor(
        private router: Router,
    ) {

    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = localStorage.getItem(`token`);
        let modReq = null;

        if (token) {
            const authHeader = 'Bearer ' + token;
            const updUrl = { url: req.url, headers: req.headers.set('Authorization', authHeader) };
            modReq = req.clone(updUrl);
        } else {
            const url = { url: req.url };
            modReq = req.clone(url);
        }

        return next.handle(modReq).do((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
                // do stuff with response if you want
            }
        }, (err: any) => {
            if (err instanceof HttpErrorResponse) {
                if (err.status === 401) {
                    console.log(`401`);
                }
                if (err.status === 403) {
                    console.log(`403`);
                }
            }
        });
    }
}
