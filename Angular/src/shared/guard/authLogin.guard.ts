import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ProfilService } from '../services/ProfilService.services';

@Injectable({
    providedIn: 'root'
})
export class AuthLoginGuard implements CanActivate {

    constructor(private profilService: ProfilService,
                private router: Router) { }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (this.profilService.isLogged()) {
            this.router.navigate(['/']);
            return false;
        }
        else {
            return true;
        }
    }
}
