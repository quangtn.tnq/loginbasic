import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ApiEndpointInterceptor } from 'src/shared/http-interceptor/api-endpoint.interceptor';
import { ProfilService } from 'src/shared/services/ProfilService.services';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    ProfilService,
    { provide: HTTP_INTERCEPTORS, useClass: ApiEndpointInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
