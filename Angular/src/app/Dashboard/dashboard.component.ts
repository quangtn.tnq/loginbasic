import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProfilService } from 'src/shared/services/ProfilService.services';
import { ProfilViewModel } from 'src/shared/viewModels/Profil.viewModel';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  profilViewModel: ProfilViewModel = new ProfilViewModel();

  constructor(
    private router: Router,
    private profilService: ProfilService,
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  async getData() {
    const res = await this.profilService.getDataById();
    if (!res.e) {
      this.profilViewModel = res.d;
    }
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }
}
