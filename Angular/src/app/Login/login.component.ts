import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginModel } from 'src/shared/models/Login.model';
import { VerifyOAuthModel } from 'src/shared/models/VerifyOAuth.model';
import { ProfilService } from 'src/shared/services/ProfilService.services';
import { OAuthViewModel } from 'src/shared/viewModels/OAuth.viewModel';
import { Md5 } from 'ts-md5';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginModel: LoginModel = new LoginModel();
  oAuthViewModel: OAuthViewModel = new OAuthViewModel();

  verifyOAuthModel: VerifyOAuthModel = new VerifyOAuthModel();

  password = ``;
  message = ``;
  isError = false;
  isVerifyOAuth = false;
  
  constructor(
    private router: Router,
    private profilService: ProfilService,
  ) { }

  ngOnInit(): void {
    
  }

  async loginSubmit() {
    this.isError = false;
    this.message = ``;
    if (!this.loginModel.username) {
      this.message = `Enter an UserName`;
      this.isError = true;
      return;
    }
    if (!this.password) {
      this.message = `Enter an Password`;
      this.isError = true;
      return;
    }
    const md5ReNew = new Md5();
    var pass = md5ReNew.appendStr(`${this.password}`).end() as any;
    this.loginModel.password = pass;
    const res = await this.profilService.login(this.loginModel);
    if (!res.e) {
      await this.createOAuth();
    }
  }

  async createOAuth() {
    const res = await this.profilService.createOAuth();
    if (!res.e) {
      this.oAuthViewModel = res.d;
      this.verifyOAuthModel.key = res.d.Key;
      this.isVerifyOAuth = true;
    }
  }
  async verifyOAuth() {
    const res = await this.profilService.verifyOAuth(this.verifyOAuthModel);
    if (!res.e) {
      this.isVerifyOAuth = false;
      localStorage.setItem(`isVerifyOAuth`, `${this.isVerifyOAuth}`);
      this.router.navigate(['/dashboard']);
    }
  }
}
