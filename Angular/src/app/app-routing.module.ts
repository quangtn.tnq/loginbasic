import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/shared/guard/auth.guard';
import { AuthLoginGuard } from 'src/shared/guard/authLogin.guard';

const routes: Routes = [
  {
    path: 'login', loadChildren: () => import('./Login/login.module').then(m => m.LoginModule),
    canActivate: [AuthLoginGuard]
  },

  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { 
    path: 'dashboard', loadChildren: () => import('./Dashboard/dashboard.module').then(m => m.DashboardModule),
    canActivate: [AuthGuard], canActivateChild: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
